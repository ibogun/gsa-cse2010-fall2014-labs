
public class Memory {

	public byte[] memoryArray;
	public boolean[] occupiedMemory;		// boolean array representing which bytes are occupied

	public int indexOfFreeMemory=0; 		//  index where next object will be stored in the memory


	public Memory(int size) {
	// constructor
	}

	int allocate(int num,Class<?> cls){
	// allocate num elements of type cls in the memory array. Index of the first element should be returned.
	// Class<?> cls is simply a way to pass class type in a variable
	}
	
	// convenience function - nothing to implement here
	int allocate(Object[] a){	
		return allocate(a.length, a[0].getClass());
	}
	
	
	boolean copy(int pointer,Object[] objs){
	// copy objs into memory starting at index pointer. True should be returned if it is possible (there is enough memory to store all the objects), false otherwise.
	}
	
	public byte[][] read(int pointer,Class<?> cls, int num){
	// read array of size num of  the objects of type cls starting at pointer and return it
	}	
	
	public void free(){
	// free everything from the memory
	}
	
	public void free(int pointer,Class<?> cls,int num){
	// free objects array of size num of type cls starting at pointer
	}
	
	public String toString() {
	// print out which elements of the memory array are occupied and which are not
	}

	String printLocationInMemory(int pointer,Class<?> cls, int num){
	// return a string representing location in the memory of object array of size num of type cls starting at pointer
	}
	
	
	public int addIntArrays(int pointer1,int pointer2,int num){
	//Given two pointers of Integer array of the same size add them, allocate and store result in the memory array and return it's pointer
	}


}