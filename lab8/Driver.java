
public class Driver {

	
	public static void main(String[] args) {
		RedBlackTree<Integer> tree = new RedBlackTree<Integer>();

		int n=8;

		for (int i = 0; i < n; i++) {
			tree.insert(i);
		}

		System.out.println(tree);

		System.out.println(tree.checkIfRBPropertySatisfied());

		
		tree.delete(5);

		System.out.println(tree);
		System.out.println(tree.checkIfRBPropertySatisfied());

		System.out.println(tree.inorderTreeWalk());
	}
}
