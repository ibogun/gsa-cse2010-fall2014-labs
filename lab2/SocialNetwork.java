
import java.util.ArrayList;

public class SocialNetwork implements Network{
	
	//maximum number of people in the network
	int n;
	
	// names of the people in the network
	ArrayList<String> names;

	// constructor
	public SocialNetwork(int n_) {
	}

	// Add a friendship between personAname - personBname into the network
	public void addPeople(String personAname, String personBname) {

	}


	// process multiple friendship requests from a multiline string where
	// each line is in the form:  personA - personB
	// Hint: this function should be using addPeople()
	public void processConnections(String multiStringWithConnections) {
	}


	// given a name return an array of people who should be invited to the party
	public String[] inviteToParty(String name) {

	}

	// return true if there is a link of friends between personAname and personBname
	public boolean areConnected(String personAname, String personBname) {

	}
	
	// print return a string containing communities of the network
	public String printCommunities() {

	}
	
	
	
}
