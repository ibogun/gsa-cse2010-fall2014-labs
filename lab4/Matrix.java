public class Matrix {

	private double[][] elements; 			// elements of the matrix
	private int n;							// size
	
	
	public Matrix(double[][] elements_) {
		// constructor
	}
	
	public Matrix multiplyStrassen(Matrix b) {
	// implement Strassen method for matrix multiplication. This function should be recursive.	
	}
	
	public Matrix multiply(Matrix b) {
	// 	implement regular matrix multiplication method (hint: you might want to use it for testing)
	}
	
	public boolean equals(Matrix b) {
	// check if matrices are equal. Compare elements up to certain precision, say 1e-6, e.g.
	// abs(this.elements[i][j]-b.elements[i][j])<1e-6	
	}
	
	public String toString() {
	// return string representation of the matrix	
	}
	
	public Matrix add(Matrix b) {
	// addition
	}
	
	public Matrix subtract(Matrix b) {
	// multiplication
	}
	
}