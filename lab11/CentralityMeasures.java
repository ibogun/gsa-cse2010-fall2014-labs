
public class CentralityMeasures {


	private double[][] D;
    private Integer[][] Pi;
    
    
    private Graph g;
	
    public double[][] getD() {
		return D;
	}

	public void setD(double[][] d) {
		D = d;
	}

	public Integer[][] getPi() {
		return Pi;
	}

	public void setPi(Integer[][] pi) {
		Pi = pi;
	}

	public CentralityMeasures(Graph g_) {
		this.g=g_;
	}

	public void calculateAllPairsShortestPaths() {
		// calculate all pairs shortest path 
	}

	public  double betweenness( int k) {
	// calculate betweenness for the vertex k
	}
	
	public  double closeness(int i) {
	// calculate closeness for the vertex i
	}



	public  void printMatrix(Integer[][] M) {

		for (int i = 0; i < M.length; i++) {
			for (int j = 0; j < M.length; j++) {


				System.out.print(M[i][j] + "  ");

			}
			System.out.println("");
		}
	}

	public  void printMatrix(double[][] M) {

		for (int i = 0; i < M.length; i++) {
			for (int j = 0; j < M.length; j++) {
				if (M[i][j] == Integer.MAX_VALUE) {
					System.out.print("Inf  ");
				} else {
					System.out.print((double)Math.round(M[i][j]*100)/100 + "  ");
				}

			}
			System.out.println("");
		}
	}

}
