public class Driver{
	
	public static void main(String[] args) {

		String dictionaryFilename="/Users/Ivan/Dropbox/GSA/Fall_2014/gsa-fall2014-labs/src/lab9_Edit_distance/words.txt";
		
		int shingleSize=4;
		SpellCheck d = new SpellCheck(dictionaryFilename,shingleSize);
		
		System.out.println(d.distance("kittens", "sittings")); 
		System.out.println(d.distance("data structures", "algorithms")); 
		System.out.println(d.distance("Monday", "Friday"));
		
		
		String strToCorrect="buildinggg";
		
		long t1,t2;
		Pair<String, Integer> correction;
		
		t1=System.currentTimeMillis();
		correction=d.naiveCorrect(strToCorrect);
		System.out.println(correction);
		t2=System.currentTimeMillis();
		
		System.out.println("Time for naive approach: "+(t2-t1));
		
		t1=System.currentTimeMillis();
		correction=d.fastCorrect(strToCorrect);
		System.out.println(correction);
		t2=System.currentTimeMillis();
		
		System.out.println("Time for shingle-based approach:" +(t2-t1));
	
		
	}
}