
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;


public class SpellCheck {
	
	HashSet<String> dictionary;
	
	HashTable<String, String> shinglesDict;    // use dictionarySize*4 as the number of linkedlists
	
	private  final int dictionarySize=899981;  // use this number to initialize dictionary


	
	public SpellCheck(String dictionaryFilename, int shingleSize){
		// constructor which takes in name of the dictionary and shingle size
	}

	
	public int distance(String s, String t){
		// calculate edit distance for strings s,t. Solution has to run in O(max{s.length,t.length}) time and space
	}
	
	
	public Pair<String, Integer> naiveCorrect(String wordToCorrect){
		// calculate best correction word using naive approach
	}
	
	
	public Pair<String, Integer> fastCorrect(String word){
		// calculate best correction using shingles approach
	}


}
