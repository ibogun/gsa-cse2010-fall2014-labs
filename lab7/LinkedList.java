
import java.util.HashSet;
import java.util.Set;


public class LinkedList<Key, Value> {

    private int N;           // number of key-value pairs
    private Node first;      // the linked list of key-value pairs

    // a helper linked list data type
    private class Node {

        private Key key;		
        private Value val;
        private Node next;		// pointer to the next Node

        public Node(Key key, Value val, Node next) {
            this.key = key;
            this.val = val;
            this.next = next;
        }
    }

    public void add(Key key, Value val) {
		// add new Key-Value pair to the linked list
    }

    // return number of key-value pairs
    public int size() {
        return N;
    }

    // is the symbol table empty?
    public boolean isEmpty() {
        return size() == 0;
    }

    public boolean contains(Key key) {
        // return true if the key is in the list, false otherwise
    }

   
    public Value get(Key key) {
		 // return the value associated with the key, or null if the key is not present
    }

  
    public void put(Key key, Value val) {
 	   // add a key-value pair, replacing old key-value pair if key is already present
    }
    
    
    public HashSet<Key> getKeys(){
    	// return set of unique keys. Nothing to implement here
    	Node firstNode=first;
    	
    	HashSet<Key> s = new HashSet<Key>();
    	
    	while(firstNode!=null){
    		s.add(firstNode.key);
    		firstNode=firstNode.next;
    	}
    	return s;
    }

   
    public void delete(Key key) {
         // remove key-value pair with given key
    }


    public String toString() {
		//return string representation of the list. Nothing to implement here.
		
        Node var = this.first;
        String result="";
        while (var != null) {
            result+=var.key.toString() + "\t \t " + var.val+"\n";
            var = var.next;
        }
        
        return result;
    }

    public static void main(String args[]) {
        LinkedList<String, Integer> list = new LinkedList<String, Integer>();
        list.add("asd", 5);
        System.out.println(list);
    }
}
